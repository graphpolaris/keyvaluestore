/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package keyvaluestore

import "context"

/*
Delete deletes the key value pair from the key value store
	context: context.Context, the context in which the delete operation should take place
	key: string, the key for which to delete the value from the key value store
	Returns: a possible error
*/
func (d *Driver) Delete(ctx context.Context, key string) error {
	return d.client.Del(ctx, key).Err()
}
