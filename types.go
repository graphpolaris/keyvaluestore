/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package keyvaluestore

import "errors"

/*
Type describes the return type expected from the key value store
*/
type Type int

const (
	// String is a string
	String Type = iota
	// Int is a int
	Int
	// Float32 is a float32
	Float32
	// Float64 is a float64
	Float64
	// Bool is a bool
	Bool
	// Bytes is a byte array
	Bytes
)

// ErrKeyNotFound is self explanatory
var ErrKeyNotFound = errors.New("key not present in kvs")
var KeyAlreadyExists = errors.New("key already present in kvs")
