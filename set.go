/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package keyvaluestore

import (
	"context"
	"os"
	"time"

	"github.com/rs/zerolog/log"
)

/*
Set sets the value for the given key

	ctx: context.Context, the context in which to perform the set operation
	key: string, the key for which you want to store the value
	value: interface{}, the value you want to store
	Return: an error if something goes wrong
*/
func (d *Driver) SetForever(ctx context.Context, key string, value interface{}) error {
	return d.Set(ctx, key, value, 0)
}

/*
SetMapWithEnvDuration sets the map for the given key with the expiration time set by an environment variable

	ctx: context.Context, the context in which to perform the set operation
	key: string, the key for which you want to store the value
	value: map[string]interface{}, the value you want to store
	expirationEnvVar: string, the environment variable that contains the expiration time
	expirationFallback: time.Duration, the fallback expiration time
	Return: an error if something goes wrong
*/
func (d *Driver) SetWithEnvDuration(ctx context.Context, key string, value interface{}, expirationEnvVar string, expirationFallback time.Duration) error {
	duration := expirationFallback
	if value, ok := os.LookupEnv(expirationEnvVar); ok {
		_duration, err := time.ParseDuration(value)
		if err != nil {
			return err
		}

		duration = _duration
	} else {
		log.Warn().Msgf("Environment variable %s not set, using fallback duration %s", expirationEnvVar, expirationFallback)
	}

	return d.Set(ctx, key, value, duration)
}

/*
ExtendExpiration extends the expiration time of the value for the given key

	ctx: context.Context, the context in which to perform the set operation
	key: string, the key for which you want to store the value
	expiration: time.Duration, the expiration time of the value
	Return: an error if something goes wrong
*/
func (d *Driver) ExtendExpiration(ctx context.Context, key string, expirationEnvVar string, expirationFallback time.Duration) error {
	duration := expirationFallback
	if value, ok := os.LookupEnv(expirationEnvVar); ok {
		_duration, err := time.ParseDuration(value)
		if err != nil {
			return err
		}

		duration = _duration
	} else {
		log.Warn().Msgf("Environment variable %s not set, using fallback duration %s", expirationEnvVar, expirationFallback)
	}

	status := d.client.Expire(ctx, key, duration)
	return status.Err()
}

/*
SetMapWithEnvDuration sets the map for the given key with the expiration time set by an environment variable

	ctx: context.Context, the context in which to perform the set operation
	key: string, the key for which you want to store the value
	value: map[string]interface{}, the value you want to store
	expirationEnvVar: string, the environment variable that contains the expiration time
	expirationFallback: time.Duration, the fallback expiration time
	Return: an error if something goes wrong
*/
func (d *Driver) SetMapWithEnvDuration(ctx context.Context, key string, value map[string]interface{}, expirationEnvVar string, expirationFallback time.Duration) error {
	return d.SetWithEnvDuration(ctx, key, value, expirationEnvVar, expirationFallback)
}

/*
Set sets the value for the given key

	ctx: context.Context, the context in which to perform the set operation
	key: string, the key for which you want to store the value
	value: interface{}, the value you want to store
	expiration: time.Duration, the expiration time of the value
	Return: an error if something goes wrong
*/
func (d *Driver) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) error {
	status := d.client.Set(ctx, key, value, expiration)
	return status.Err()
}

/*
SetMap sets the map for the given key

	ctx: context.Context, the context in which to perform the set operation
	key: string, the key for which you want to store the value
	value: map[string]interface{}, the value you want to store
	expiration: time.Duration, the expiration time of the value
	Return: an error if something goes wrong
*/
func (d *Driver) SetMap(ctx context.Context, key string, value map[string]interface{}, expiration time.Duration) error {
	return d.Set(ctx, key, value, expiration)
}
