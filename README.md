# KeyValueStore Driver
This is the keyvaluestore driver package. It is used as a middleware between Redis and our service usecases. The `Interface` in `interface.go` is implemented by both the actual driver, as well as a mocked driver, to allow for a mocked keyvaluestore to be injected into tests.

## Creating a Keyvaluestore
```go
import "git.science.uu.nl/datastrophe/keyvaluestore"

keyValueStore := keyvaluestore.NewDriver()

keyValueStore.Connect()
```

## Creating a Mock Keyvaluestore
```go
import "git.science.uu.nl/datastrophe/keyvaluestore"

mockKeyValueStore := keyvaluestore.NewMockDriver()
```

## Setting Key -> Value
```go
import "git.science.uu.nl/datastrophe/keyvaluestore"

keyValueStore.Set(context.Background(), "key", "value")
```

## Getting Value for Key
```go
import "git.science.uu.nl/datastrophe/keyvaluestore"

keyValueStore.Get(context.Background(), "key", keyvaluestore.String)
```

## Testing
To test this package we wrote a bash script. This script starts up a Redis Docker container and then runs the package tests on that container. Once all tests have run the Docker container is killed and deleted.