/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package keyvaluestore

import (
	"context"
	"time"
)

/*
Interface is an interface for a key value storage
*/
type Interface interface {
	Connect(address string, password string) error
	GetMap(ctx context.Context, key string) (value map[string]interface{}, err error)
	Get(ctx context.Context, key string, t Type) (value interface{}, err error)
	SetForever(ctx context.Context, key string, value interface{}) error
	SetMap(ctx context.Context, key string, value map[string]interface{}, expiration time.Duration) error
	Set(ctx context.Context, key string, value interface{}, expiration time.Duration) error
	SetMapWithEnvDuration(ctx context.Context, key string, value map[string]interface{}, expirationEnvVar string, expirationFallback time.Duration) error
	SetWithEnvDuration(ctx context.Context, key string, value interface{}, expirationEnvVar string, expirationFallback time.Duration) error
	ExtendExpiration(ctx context.Context, key string, expirationEnvVar string, expirationFallback time.Duration) error
	Delete(ctx context.Context, key string) error
	Disconnect() error
	isConnected() bool
}
