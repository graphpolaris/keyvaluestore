module git.science.uu.nl/graphpolaris/keyvaluestore

go 1.22

require (
	github.com/go-redis/redis/v8 v8.11.5
	github.com/rs/zerolog v1.33.0
)

require (
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	golang.org/x/sys v0.21.0 // indirect
)
