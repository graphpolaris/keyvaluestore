/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package keyvaluestore

import (
	"context"
	"encoding/json"

	"github.com/go-redis/redis/v8"
)

/*
Get gets the value for the given key

	ctx: context.Context, the context in which to perform the get operation
	key: string, the key for which to get the value
	Returns the value retrieved from the keyvaluestore
*/
func (d *Driver) Get(ctx context.Context, key string, t Type) (value interface{}, err error) {
	switch t {
	case Int:
		value, err = d.client.Get(ctx, key).Int()
	case String:
		value, err = d.client.Get(ctx, key).Val(), nil
	case Float32:
		value, err = d.client.Get(ctx, key).Float32()
	case Float64:
		value, err = d.client.Get(ctx, key).Float64()
	case Bool:
		value, err = d.client.Get(ctx, key).Bool()
	case Bytes:
		value, err = d.client.Get(ctx, key).Bytes()
	default:
		value, err = d.client.Get(ctx, key).String(), nil
	}

	if err == redis.Nil {
		err = ErrKeyNotFound
	}

	return
}

/*
GetMap gets the map for the given key

	ctx: context.Context, the context in which to perform the get operation
	key: string, the key for which to get the value
	Returns the map retrieved from the keyvaluestore
*/
func (d *Driver) GetMap(ctx context.Context, key string) (map[string]interface{}, error) {
	value := make(map[string]interface{})

	bValue, err := d.client.Get(ctx, key).Bytes()
	if err == redis.Nil {
		err = ErrKeyNotFound
	}
	err = json.Unmarshal(bValue, &value)
	if err != nil {
		return nil, err
	}

	return value, nil
}
