/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package keyvaluestore

import (
	"context"
	"os"
	"time"

	"github.com/go-redis/redis/v8"
)

/*
Driver implements the keyvaluestore interface
*/
type Driver struct {
	client *redis.Client
}

/*
NewDriver creates a new keyvaluestore driver

	Return: Interface, returns a new driver
*/
func NewDriver() Interface {
	return &Driver{}
}

/*
Connect opens the connection to the keyvaluestore

	Return: error, returns a potential error
*/
func (d *Driver) Connect(address string, password string) error {

	// Create redis client
	d.client = redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
	})

	pong := d.client.Ping(context.Background())
	return pong.Err()
}

/*
Disconnect disconnects from the keyvaluestore

	Return: error, returns a potential error
*/
func (d *Driver) Disconnect() error {
	return d.client.Close()
}

/*
checks if the driver is connected

	Return: bool, returns true if the driver is connected
*/
func (d *Driver) isConnected() bool {
	// Ping the client
	pingContext, pingCancel := context.WithTimeout(context.Background(), time.Second*5)
	defer pingCancel()
	_, err := d.client.Ping(pingContext).Result()

	if err != nil {
		return false
	}

	return true
}

func (d *Driver) getEnvDuration(key, fallback string) (time.Duration, error) {
	val := fallback
	if value, ok := os.LookupEnv(key); ok {
		val = value
	}
	return time.ParseDuration(val)
}
