/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package keyvaluestore

import (
	"context"
	"time"
)

/*
A MockDriver implements methods to set key-value data (mock)
*/
type MockDriver struct {
	Data map[string]interface{}
}

/*
NewMockDriver creates a key value store driver (mock)

	Return: the interface for the new mock driver
*/
func NewMockDriver() Interface {
	return &MockDriver{
		Data: make(map[string]interface{}),
	}
}

/*
Connect connects to the keyvaluestore

	Return: if there's an error
*/
func (kvs *MockDriver) Connect(address string, password string) error {
	return nil
}

/*
Set sets a key to a value in the key value store. Expects a non-pointer as value. (mock)

	ctx: context.Context, the context for the driver
	key: string, the key for the driver
	value: interface{}, the value for the driver
	Return: if there's an error
*/
func (kvs *MockDriver) SetForever(ctx context.Context, key string, value interface{}) error {
	kvs.Data[key] = value
	return nil
}
func (kvs *MockDriver) SetWithEnvDuration(ctx context.Context, key string, value interface{}, expirationEnvVar string, expirationFallback time.Duration) error {
	kvs.Data[key] = value
	return nil
}
func (kvs *MockDriver) SetMapWithEnvDuration(ctx context.Context, key string, value map[string]interface{}, expirationEnvVar string, expirationFallback time.Duration) error {
	kvs.Data[key] = value
	return nil
}
func (kvs *MockDriver) SetMap(ctx context.Context, key string, value map[string]interface{}, expiration time.Duration) error {
	kvs.Data[key] = value
	return nil
}
func (kvs *MockDriver) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) error {
	kvs.Data[key] = value
	return nil
}

/*
Get gets the value for the supplied key from the key value store (mock)

	ctx: context.Context, the context for the driver
	key: string, the key for the driver
	t: Type, the type of the driver
	Return: (interface{}, error), the interface and any error that occurred
*/
func (kvs *MockDriver) Get(ctx context.Context, key string, t Type) (interface{}, error) {
	return kvs.Data[key], nil
}
func (kvs *MockDriver) GetMap(ctx context.Context, key string) (map[string]interface{}, error) {
	return kvs.Data[key].(map[string]interface{}), nil
}

/*
Disconnect disconnects from the key value store (mock)

	Return: if there's an error
*/
func (kvs *MockDriver) Disconnect() error {
	return nil
}

/*
isConnected checks if it is connected

	Return, returns true
*/
func isConnected() bool {
	return true
}

/*
Delete deletes the key value pair from the key value store (mock)

	ctx: context.Context, the context for the driver
	key: string, the key for the driver
	Return: if there's an error
*/
func (kvs *MockDriver) Delete(ctx context.Context, key string) error {
	delete(kvs.Data, key)
	return nil
}

/*
isConnected checks if it is connected

	Return: returns true
*/
func (kvs *MockDriver) isConnected() bool {
	return true
}

/*
ExtendExpiration extends the expiration time of the value for the given key

	ctx: context.Context, the context in which to perform the set operation
	key: string, the key for which you want to store the value
	expiration: time.Duration, the expiration time of the value
	Return: an error if something goes wrong
*/
func (kvs *MockDriver) ExtendExpiration(ctx context.Context, key string, expirationEnvVar string, expirationFallback time.Duration) error {
	return nil
}
